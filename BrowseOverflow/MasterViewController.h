//
//  MasterViewController.h
//  BrowseOverflow
//
//  Created by Eric Crawford on 12/12/13.
//  Copyright (c) 2013 Eric Crawford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController

@end
