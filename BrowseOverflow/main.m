//
//  main.m
//  BrowseOverflow
//
//  Created by Eric Crawford on 12/12/13.
//  Copyright (c) 2013 Eric Crawford. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
